package com.paic.arch.interviews;



/**
 * Created by benjamin on 05/04/2018.
 */
public class TimeConverterImpl implements TimeConverter {

    @Override
    public String convertTime(String aTime) {

        //extract the hours, minutes and seconds from a time string
        String[] time = aTime.split(":");

        //convert the strings into int
        int hour = Integer.parseInt(time[0]);
        int min = Integer.parseInt(time[1]);
        int sec = Integer.parseInt(time[2]);

        StringBuilder sb = new StringBuilder();

        // Handling seconds
        sb.append(sec % 2 == 0 ? "Y" : "O");
        sb.append("\n");

        // Handling hours
        char[] hour1 = {'O', 'O', 'O', 'O'}; // hour line 1
        char[] hour2 = {'O', 'O', 'O', 'O'}; // hour line 2

        int j = 0; //the pointer of char array

        // give a red light on first row of hour light for every 5 hour value
        for ( int i = 5; i <= hour; i += 5 ) {
            hour1[j] = 'R';
            j++;
        }

        // give a red light on second row of hour light for every 1 hour value
        for ( int i = 0; i < hour % 5; i++ ) {
            hour2[i] = 'R';
        }

        sb.append(new String(hour1)).append("\n");
        sb.append(new String(hour2)).append("\n");

        // Handling minutes
        char[] min1 = {'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O', 'O'};
        char[] min2 = {'O', 'O', 'O', 'O'};

        j = 0; // reset the array pointer

        // 1) give a yellow light on the first row of minutes lights for every 5 minutes value
        // 2) give a red light instead of yellow light for every 15 minutes value
        for ( int i = 5; i <= min; i += 5 ) {
            min1[j] = (i % 15 == 0 ? 'R' : 'Y');
            j++;
        }

        // give a yellow light on the second row of minutes lights for every 1 minute
        for ( int i = 0; i < min % 5; i++ ) {
            min2[i] = 'Y';
        }

        sb.append(new String(min1)).append("\n");
        sb.append(new String(min2));


        return sb.toString();
    }
}
